<?php
namespace App\Message;

if(!isset($_SESSION['message']))
    session_start();

class Message{
    public static function message($data=NULL){
        if(!empty($data)){
            self::setMessage($data);
        }

        else{
            $message = self::getMessage();
            return $message;
        }
    }


    public static function getMessage(){
        $msg = $_SESSION['message'];
        $_SESSION['message']= "";
        return $msg;
    }


    public static function setMessage($msg){
        $_SESSION['message'] = $msg;
    }
}