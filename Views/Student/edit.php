<?php

include_once('../../vendor/autoload.php');
use App\Student\Student;





$obj=new Student();
$obj->prepare($_GET);
$single= $obj->view();

?>



<html>
<head>
    <title>View Student Details</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2>View Student details</h2>
    <form action="update.php" method="post">
        <input type="hidden" name="id" value="<?php echo $_GET['id']?>">
        <label>Student name :</label> <br>
        <input type="text" name="name" value="<?php echo $single['name'] ?>"><br><br>
        <label>Selected courses:</label><br>
        <div class="checkbox">
            <label><input type="checkbox" name="course[]" value="PHP" <?php if(in_array("PHP",$single['course'])): ?> checked <?php endif ?>>PHP</label>
        </div>
        <div class="checkbox">
            <label><input type="checkbox" name="course[]" value="JAVA"  <?php if(in_array("JAVA",$single['course'])): ?> checked <?php endif ?>>JAVA</label>
        </div>
        <div class="checkbox">
            <label><input type="checkbox" name="course[]" value="PYTHON"  <?php if(in_array("PYTHON",$single['course'])): ?> checked <?php endif ?>>PYTHON</label>
        </div>
        <div class="checkbox">
            <label><input type="checkbox" name="course[]" value="HTML"  <?php if(in_array("HTML",$single['course'])): ?> checked <?php endif ?> >HTML</label>
        </div>
        <div class="checkbox">
            <label><input type="checkbox" name="course[]" value="ORACLE"  <?php if(in_array("ORACLE",$single['course'])): ?> checked <?php endif ?>>ORACLE</label>
        </div>
        <div class="checkbox">
            <label><input type="checkbox" name="course[]" value="DOT NET" <?php if(in_array("DOT NET",$single['course'])): ?> checked <?php endif ?> >DOT NET</label>
        </div>
        <input type="submit" value="Submit">
    </form>
</div>

</body>
</html>



